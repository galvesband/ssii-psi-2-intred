package ssii.psi2.intred;

import java.io.*;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * <p>Genera informes con indicadores a partir del log del servidor.</p>
 *
 * <p>El log del servidor guarda una entrada por cada mensaje, indicando si ha sido exitoso
 * o no y un código con el tipo de incidente o resultado, además de la fecha y hora de la comunicación.</p>
 *
 * <p>Esta clase contiene los métodos necesarios para leer dicho archivo, interpretar la información
 * y producir un archivo de indicadores en el que se mostrará el número de mensajes y el ratio de errores
 * por periodo (argumento), y un indicador de la tendencia del ratio de error.</p>
 */
public class ReportGenerator {
    /**
     * Tipos de incidente que puede indicar una entrada del log.
     */
    private enum IncidentType {
        /**
         * Mensaje correcto
         */
        SUCCESS,
        /**
         * Error desconocido (como por ejemplo el cierre inesperado de la conexión).
         */
        UNKNOWN,
        /**
         * Error en la comprobación de integridad del mensaje.
         */
        INTEGRITY_ERROR,
        /**
         * El challengeNonce o el dayNonce son incorrectos.
         */
        NONCE_ERROR,
        /**
         * El mensaje tiene un formato no soportado o erroneo.
         */
        PROTOCOL_ERROR,
        /**
         * La operación especificada en el mensaje es desconocida.
         */
        UNKNOWN_OPERATION,
        /**
         * Al procesar el mensaje (tras validarlo), no se pudo completar.
         */
        OPERATION_ERROR
    }

    /**
     * Estructura para almacenar la información de una entrada del log parseada.
     */
    private static class LogEntry implements Comparable<LogEntry> {
        /**
         * Fecha y hora del mensaje
         */
        LocalDateTime timestamp;
        /**
         * Dirección ip del emisor del mensaje.
         */
        Inet4Address host = null;
        /**
         * Contenido del mensaje o descripción del error desconocido (UNKNOWN_ERROR).
         */
        String message;
        /**
         * Tipo de incidente en el mensaje.
         */
        IncidentType incidentType;

        @Override
        public int compareTo(LogEntry logEntry) {
            return timestamp.compareTo(logEntry.timestamp);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            LogEntry logEntry = (LogEntry) o;

            return timestamp != null ? timestamp.equals(logEntry.timestamp) : logEntry.timestamp == null;
        }

        @Override
        public int hashCode() {
            return timestamp.hashCode();
        }
    }

    /**
     * Estructura para almacenar la información de un indicador del informe.
     */
    private static class Indicator {
        /**
         * Fecha y hora del inicio del periodo que comprende el indicador.
         */
        LocalDateTime fromTimestamp;
        /**
         * Fecha y hora del final del periodo que comprende el indicador.
         */
        LocalDateTime toTimestamp;
        /**
         * Número de mensajes del periodo.
         */
        int messageCount;
        /**
         * Ratio de error (tanto por uno).
         */
        float failRate;

        public Indicator(LocalDateTime fromTimestamp, int periodInMinutes) {
            this.fromTimestamp = fromTimestamp;
            toTimestamp = this.fromTimestamp.plusMinutes(periodInMinutes);
            messageCount = 0;
            failRate = 0;
        }
    }

    /**
     * <p>Lee el fichero de log del servidor y devuelve un array de LogEntry.</p>
     *
     * @param filePath Ruta al archivo de logs del servidor.
     * @return Array con las entradas del log.
     * @throws IOException No se ha podido abrir o leer el archivo.
     */
    public static LogEntry[] parseLogFile(String filePath) throws IOException {
        List<LogEntry> rvalList = new ArrayList<>();

        File logFile = new File(filePath);
        if (!logFile.exists())
            throw new FileNotFoundException("File not found: " + filePath);

        BufferedReader fileStream = null;
        try {
            fileStream = new BufferedReader(new FileReader(logFile));
            String line = fileStream.readLine();
            while (line != null) {
                rvalList.add(parseLogLine(line));
                line = fileStream.readLine();
            }
        } finally {
            if (fileStream != null) {
                fileStream.close();
            }
        }

        Collections.sort(rvalList);
        LogEntry [] rval = new LogEntry[rvalList.size()];
        rval = rvalList.toArray(rval);

        return rval;
    }

    /**
     * Interpreta una línea de log del servidor y devuelve una estructura LogEntry con su información.
     *
     * @param line Línea del log del servidor.
     * @return Información de la línea de log.
     */
    private static LogEntry parseLogLine(String line) {
        LogEntry rval = new LogEntry();

        String[] blocks = line.split(" -- ");
        if (blocks.length != 2) {
            throw new IllegalArgumentException("Invalid log line (missing ' -- ' separator): " + line);
        }
        String[] fields = blocks[0].split(" ");
        if (fields.length != 4) {
            throw new IllegalArgumentException("Invalid log line (unexpected number of fields in first block): " + line);
        }

        rval.message = blocks[1];

        boolean isOk = false;
        switch (fields[0]) {
            case "OK":
                isOk = true;
                break;
            case "ER":
                isOk = false;
                break;
            default:
                throw new IllegalArgumentException("Invalid log line (unknown status '" + fields[0] + "': " + line);
        }

        try { rval.host = (Inet4Address) InetAddress.getByName(fields[1]); }
        catch (UnknownHostException ue) { throw new IllegalArgumentException("Invalid log line (invalid host name): " + line); }

        try { rval.timestamp = LocalDateTime.parse(fields[2], DateTimeFormatter.ISO_LOCAL_DATE_TIME); }
        catch (Exception e) { throw new IllegalArgumentException("Invalid log line (invalid timestamp): " + line); }

        switch (fields[3]) {
            case "SUCCESS":
                rval.incidentType = IncidentType.SUCCESS;
                break;
            case "UNKNOWN":
                rval.incidentType = IncidentType.UNKNOWN;
                break;
            case "INTEGRITY_ERROR":
                rval.incidentType = IncidentType.INTEGRITY_ERROR;
                break;
            case "NONCE_ERROR":
                rval.incidentType = IncidentType.NONCE_ERROR;
                break;
            case "PROTOCOL_ERROR":
                rval.incidentType = IncidentType.PROTOCOL_ERROR;
                break;
            case "UNKNOWN_OPERATION":
                rval.incidentType = IncidentType.UNKNOWN_OPERATION;
                break;
            case "OPERATION_ERROR":
                rval.incidentType = IncidentType.OPERATION_ERROR;
                break;
            default:
                throw new IllegalArgumentException("Invalid log line (unknown incident type): " + line);
        }

        return rval;
    }

    /**
     * <p>Procesa un archivo de log de servidor y devuelve un array de indicadores (Indicator).</p>
     *
     * @param logFilePath Ruta al archivo de log del servidor.
     * @param indicatorPeriodInMinutes Minutos que dura el periodo de tiempo que comprende cada indicador.
     * @return Array de indicadores.
     * @throws IOException Ha habido un problema al leer el archivo de log.
     */
    public static Indicator[] processLog(String logFilePath, int indicatorPeriodInMinutes) throws IOException {
        return processLog(parseLogFile(logFilePath), indicatorPeriodInMinutes);
    }

    /**
     * <p>Procesa un array de LogEntry y devuelve un array de indicadores (Indicator).</p>
     *
     * @param entries Array con las entradas del log del servidor.
     * @param indicatorPeriodInMinutes Minutos que dura el periodo de tiempo que comprende cada indicador.
     * @return Array de indicadores.
     */
    public static Indicator[] processLog(LogEntry[] entries, int indicatorPeriodInMinutes) {
        if (entries.length == 0)
            throw new IllegalArgumentException("There should be at least one log entry.");
        if (indicatorPeriodInMinutes < 1)
            throw new IllegalArgumentException("Time period should be at least one minute.");

        List<Indicator> rvalList = new ArrayList<>();

        Indicator currentIndicator = new Indicator(entries[0].timestamp, indicatorPeriodInMinutes);
        int currentIndicatorFailCount = 0;
        // Para cada entrada del log
        for (LogEntry entry : entries) {
            // Antes de procesar la entrada, comprobamos si pertenece al periodo del indicador actual
            // Si no pertenece, tenemos que cerrar el indicador actual (calcular failRatio) y comenzar uno nuevo.

            // Si la entrada actual ocurrió después del final del periodo actual...
            if (entry.timestamp.compareTo(currentIndicator.toTimestamp) >= 0) {
                // Hay que terminar el periodo actual (calcular failRatio)
                currentIndicator.failRate = ((float) currentIndicatorFailCount / (float) currentIndicator.messageCount);
                rvalList.add(currentIndicator);

                // Empezamos el siguiente periodo.
                LocalDateTime nextIndicatorStart = currentIndicator.toTimestamp;
                while (entry.timestamp.compareTo(nextIndicatorStart.plusMinutes(indicatorPeriodInMinutes)) > 0)
                    nextIndicatorStart = nextIndicatorStart.plusMinutes(indicatorPeriodInMinutes);
                currentIndicator = new Indicator(nextIndicatorStart, indicatorPeriodInMinutes);
                currentIndicatorFailCount = 0;
            }

            // Procesamos la entrada del log en el periodo actual
            currentIndicator.messageCount++;
            switch (entry.incidentType) {
                case SUCCESS:
                case OPERATION_ERROR:
                    // Noop: la operación fue correcta a nivel de integridad y nonces.
                    break;
                case UNKNOWN:
                case INTEGRITY_ERROR:
                case NONCE_ERROR:
                case PROTOCOL_ERROR:
                case UNKNOWN_OPERATION:
                    // Se produjo un error en el protocolo (integridad, nonces, petición mal formada, desconexión inesperada, ...)
                    currentIndicatorFailCount++;
                    break;
            }
        }
        currentIndicator.failRate = ((float) currentIndicatorFailCount / (float) currentIndicator.messageCount);
        rvalList.add(currentIndicator);

        Indicator[] rval = new Indicator[rvalList.size()];
        rval = rvalList.toArray(rval);
        return rval;
    }

    private static class ReportOptions {
        String logFilePath;
        String outputFilePath;
        int periodLengthInMinutes;

        ReportOptions() {
            logFilePath = System.getProperty("user.dir") + File.separator + "server.log";
            outputFilePath = System.getProperty("user.dir") + File.separator + "server.report";
            periodLengthInMinutes = 60;
        }
    }

    /**
     * <p>Punto de entrada para el generador de informes.</p>
     *
     * <p>Argumentos aceptados:</p>
     * <ul>
     *     <li>"-logFile=" seguido de la ruta al archivo de logs del servidor.</li>
     *     <li>"-period=" seguido de un entero que indica el número de minutos que comprende cada indicador.</li>
     *     <li>"-outputFile=" seguido de la ruta al archivo de salida (donde se escribirán los indicadores).</li>
     * </ul>
     *
     * @param args Lista de argumentos.
     */
    public static void main(String[] args) {
        if (args.length == 1 && args[0] == "help") {
            printHelp();
            return;
        }

        ReportOptions options = new ReportOptions();
        for (String arg : args) {
            if (arg.startsWith("-logFile="))
                options.logFilePath = arg.replace("-logFile=", "");
            else if (arg.startsWith("-period=")) {
                try { options.periodLengthInMinutes = Integer.parseInt(arg.replace("-period=", "")); }
                catch (Exception e) { System.err.println("Invalid period: should be a positive non-zero integer (minutes)."); }
            }
            else if (arg.startsWith("-outputFile="))
                options.outputFilePath = arg.replace("-outputFile=", "");
            else {
                System.err.println("Unknown argument: " + arg);
                System.out.println("Usage: intred-report -logFile=<path> -period=<periodInMinutes>");
                return;
            }
        }

        Indicator[] indicators;
        try {
            indicators = processLog(options.logFilePath, options.periodLengthInMinutes);
        } catch (IOException e) {
            System.err.println("Error reading log file: " + options.logFilePath);
            System.err.println(e.getMessage());
            return;
        }

        BufferedWriter output = null;
        try {
            output = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new File(options.outputFilePath))));

            for (int i = 0; i < indicators.length; i++) {
                Indicator currentIndicator = indicators[i];

                output.write("-- Period from " + currentIndicator.fromTimestamp.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME) + " to " + currentIndicator.toTimestamp.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME) + "\n");
                output.write("   Message count: " + currentIndicator.messageCount + "\n");
                output.write("   Fail rate:     " + currentIndicator.failRate*100 + "%\n");
                if (i != 0) {
                    String tendency;
                    if (currentIndicator.failRate < indicators[i-1].failRate)
                        tendency = "DOWN (better)";
                    else if (currentIndicator.failRate > indicators[i-1].failRate)
                        tendency = "UP (worse)";
                    else
                        tendency = "EQUAL";
                    output.write("   Tendency:      " + tendency + "\n");
                }
                output.flush();
            }
        } catch (FileNotFoundException e) {
            // Esto no debe ocurrir porque si no existe el archivo lo creamos
            System.err.println("Unexpected file not found exception: " + e.getMessage());
            return;
        } catch (IOException e) {
            System.err.println("Error writing to the output file: " + options.outputFilePath);
            System.err.println(e.getMessage());
            return;
        } finally {
            try {
                if (output != null)
                    output.close();
            } catch (IOException e) {
                System.err.println("Error while saving output file: " + options.outputFilePath);
                System.err.println(e.getMessage());
            }
        }
    }

    private static void printHelp() {
        System.out.println("INTRed Report Generator - Generates reports from INTRed Server logs");
        System.out.println();
        System.out.println("Usage:");
        System.out.println("  -logFile=<path-to-file>       Path to the server log file to generate reports from.");
        System.out.println("  -period=<period-in-minutes>   Number of minutes every indicator last. By default: 60 (minutes)");
        System.out.println("  -outputFile=<path-to-file>    File to write indicators on.");
        System.out.println();
    }
}
