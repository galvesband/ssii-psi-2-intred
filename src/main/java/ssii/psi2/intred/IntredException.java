package ssii.psi2.intred;

/**
 * <p>Encapsula una exepción generada por una conexión en el servidor o el cliente.</p>
 */
public class IntredException extends Exception {
    /**
     * Tipo de error.
     */
    public enum ErrorCode {
        /**
         * El mensaje recibido no ha pasado el test de integridad.
         */
        INTEGRITY_ERROR,
        /**
         * El mensaje recibido contiene un dayNonce, un challengeNonce o un requestNonce inválido.
         */
        NONCE_ERROR,
        /**
         * El mensaje recibido no tiene la forma esperada.
         */
        PROTOCOL_ERROR,
        /**
         * La operación indicada en el mensaje es deconocida.
         */
        UNKNOWN_OPERATION,
        /**
         * <p>Ha ocurrido un error al ejecutar la operación.</p>
         *
         * <p>El mensaje ha pasado las pruebas de validación de integridad e incluía
         * los nonces correctos, pero al llevar a cabo la operación requerida ha
         * ocurrido un error.</p>
         */
        OPERATION_ERROR
    }

    private ErrorCode errorCode;
    private ServerConnection.RequestData request = null;

    public IntredException(ErrorCode code) {
        super(getCodeDescription(code));
        this.errorCode = code;
    }

    public IntredException(ErrorCode code, ServerConnection.RequestData request) {
        super(getCodeDescription(code));
        this.errorCode = code;
        this.request = request;
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }

    public boolean hasRequestData() {
        return request != null;
    }

    public ServerConnection.RequestData getRequestData() {
        return request;
    }

    /**
     * Devuelve una cadena breve con una descripción del error.
     *
     * @param code El error cuya descripción se desa obtener.
     * @return Cadena con la descripción del error.
     */
    public static String getCodeDescription(ErrorCode code) {
        switch (code) {
            case INTEGRITY_ERROR:
                return "Integrity check error.";
            case NONCE_ERROR:
                return  "Invalid nonce.";
            case PROTOCOL_ERROR:
                return "Malformed request.";
            case UNKNOWN_OPERATION:
                return "Request operation not supported or invalid.";
            case OPERATION_ERROR:
                return "Operation failed.";
            default:
                return "Unknown.";
        }
    }
}
