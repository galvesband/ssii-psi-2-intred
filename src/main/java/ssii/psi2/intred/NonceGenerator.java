package ssii.psi2.intred;

import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

/**
 * <p>Generación de nonces.</p>
 *
 * <p>Esta clase permite generar nonces a partir de un generador de números aleatorios seguro.</p>
 *
 * <p>En sistemas operativos UNIX y similares la fuente de datos aleatorios seguros utilizada proviene de
 *    /dev/random, lo que hace que una llamada a getBytes() de un objeto
 *    SecureRandom creado utilizando esa fuente pueda ser bloqueante si no hay entropía
 *    suficiente almacenada en el sistema. Esto puede ser un problema de disponibilidad en una aplicación
 *    de servidor.</p>
 * <p>Por eso al crear una instancia de esta clase se usa por defecto el generador SHA1PRNG, que es obligatorio en
 *    toda implementación de Java y nunca es bloqueante. Es posible forzar el uso de la fuente de entropía
 *    predeterminada del sistema si se pasa como parámetro en el constructor true.</p>
 * <p>Ver <a href="https://www.cigital.com/blog/proper-use-of-javas-securerandom/">
 *    https://www.cigital.com/blog/proper-use-of-javas-securerandom/</a>.</p>
 */
public class NonceGenerator {
    private static final String[] hexDigits ={"0", "1", "2", "3","4", "5", "6", "7","8", "9", "a", "b","c", "d", "e", "f"};
    private static final String nonNativePRNG = "SHA1PRNG";

    private SecureRandom randomizer;

    /**
     * <p>Crea un nuevo generador de nonces.</p>
     *
     * <p>
     *     Si el argumento es true el generador de datos aleatorios utilizado como fuente
     *     para generar los nonces será la opción por defecto de la combinación de plataforma y sistema operativo.
     *     Si el argumento es false se utilizará una implementación de Java (SHA1PRNG).
     * </p>
     *
     * @see NonceGenerator
     * @param useNativeRandomSource Si true, utilizará la fuente de datos aleatorios nativa del sistema.
     * @throws NoSuchAlgorithmException Algoritmo no disponible en la plataforma.
     */
    public NonceGenerator(boolean useNativeRandomSource) throws NoSuchAlgorithmException {
        init(useNativeRandomSource);
    }

    /**
     * Crea un nuevo generador de nonces basado en SHA1PRNG.
     *
     * @throws NoSuchAlgorithmException SHA1PRNG no esta disponible en la plataforma. No debería ocurrir, pues es
     * obligatorio en toda implementación de java.
     */
    public NonceGenerator() throws NoSuchAlgorithmException {
        init(false);
    }

    private void init(boolean useNativeRandomSource) throws NoSuchAlgorithmException {
        if (useNativeRandomSource) {
            randomizer = new SecureRandom();
        } else {
            randomizer = SecureRandom.getInstance(nonNativePRNG);
            // En algunas fuentes se menciona que es conveniente inicializar la semilla de SHA1PRNG de
            // forma automática mediante una llamada inicial a nextBytes. Dado que no parece tener
            // efectos perniciosos, pido un byte al randomizador.
            // Ver: http://stackoverflow.com/questions/12249235/securerandom-safe-seed-in-java
            byte b[] = new byte[1];
            randomizer.nextBytes(b);
        }
    }

    /**
     * Devuelve un entero de 32 bits aleatorio (nonce).
     *
     * @return El siguiente nonce de 32 bits.
     */
    public int getNextNonce32() {
        byte[] bytes = new byte[4];
        randomizer.nextBytes(bytes);
        return convertBytesToInt(bytes);
    }

    /**
     * Devuelve un entero de 64 bits aleatorio (nonce).
     *
     * @return El siguiente nonce de 64 bits.
     */
    public long getNextNonce64() {
        byte[] bytes = new byte[8];
        randomizer.nextBytes(bytes);
        return convertBytesToLong(bytes);
    }

    /**
     * <p>Devuelve un nonce en forma de cadena hexadecimal.</p>
     *
     * <p>La longitud del nonce depende del número de bits utilizado en la generación.
     * Cada 8 bits se traducen en 2 caraceteres hexadecimales.</p>
     *
     * @param bitCount Número de bits del nonce devuelto. Debe ser divisible por 8.
     * @return Cadena con el nuevo nonce en forma hexadecimal.
     */
    public String getNextNonceString(int bitCount) {
        if (bitCount % 8 != 0 || bitCount <= 0) {
            throw new IllegalArgumentException("'bitCount' should be greater than 0 and divisible by 8.");
        }
        int byteCount = bitCount / 8;
        byte[] bytes = new byte[byteCount];
        randomizer.nextBytes(bytes);
        return convertBytesToHexString(bytes);
    }

    /**
     * <p>Convierte 4 bytes en un entero.</p>
     *
     * <p>
     *     Dados 4 bytes con signo, devuelve un entero con signo equivalente a los 4 bytes.
     *     El byte con índice 0 será interpretado como el más significativo, mientras que
     *     el byte con índice 3 será el menos significativo.
     * </p>
     * <p>
     *     Para ello, primero convierte los 3 bytes menos significativos a bytes sin signo
     *     (de 0 a 255) y después realiza una serie de operaciones lógicas y de desplazamiento
     *     que terminan con un entero con signo.
     * </p>
     *
     * @param bytes Array de bytes a convertir en entero. Tiene que tener 4 elementos.
     * @return Entero que representa los 4 bytes.
     */
    public static int convertBytesToInt(byte[] bytes) {
        if (bytes.length != 4)
            throw new IllegalArgumentException("Array 'bytes' should have 4 bytes.");

        int rval = 0;
        for (byte i = 0; i < bytes.length; i++) {
            // Los valores negativos se convierten a positivos para hacer un entero entre 0 y 255.
            // En el caso del primer byte (índice 0), que es considerado el más significativo,
            // no se hace la conversión para que el número resultante tras las combinaciones
            // sea un entero entre Integer.MAX_VALUE e Integer.MIN_VALUE.
            // Ver test (NonceGeneratorTest).
            int tmp = (bytes[i] >= 0 || i == 0) ? (int) bytes[i] : -1*((int) bytes[i]) + 127;
            rval = rval | tmp;
            if (i != bytes.length - 1) {
                rval = rval << 8;
            }
        }

        return rval;
    }

    /**
     * Convierte 8 bytes en un entero largo.
     *
     * @see NonceGenerator#convertBytesToInt
     * @param bytes Array de bytes a convertir a entero largo. Debe tener 8 elementos.
     * @return Entero largo que representa a los 8 bytes.
     */
    public static long convertBytesToLong(byte[] bytes) {
        if (bytes.length != 8)
            throw new IllegalArgumentException("Array 'bytes' should have 8 bytes.");

        long rval = 0;
        for (byte i = 0; i < bytes.length; i++) {
            int tmp = (bytes[i] >= 0 || i == 0) ? (int) bytes[i] : -1*((int) bytes[i]) + 127;
            rval = rval | tmp;
            if (i != bytes.length - 1) {
                rval = rval << 8;
            }
        }

        return rval;
    }

    /**
     * Convierte un array de bytes en una cadena hexadecimal.
     *
     * @param hash Bytes.
     * @return Cadena hexadecimal que representa al array de bytes.
     */
    public static String convertBytesToHexString(byte[] hash)
    {
        String rval = "";
        for (byte aHash : hash) {
            rval += convertByteToHexString(aHash);
        }
        return rval;
    }

    /**
     * Convierte un byte a cadena hexadecimal.
     *
     * @param hash Byte
     * @return Cadena hexadecimal que representa el byte.
     */
    private static String convertByteToHexString(byte hash)
    {
        int n = hash;
        if (n < 0) {
            n=256 +n;
        }
        int d1 = n / 16;
        int d2 = n % 16;
        return hexDigits[d1] + hexDigits[d2];
    }
}
