package ssii.psi2.intred;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

/**
 * <p>Calcula y compara hMac de mensajes.</p>
 *
 * <p>El código de esta clase será utilizado tanto por el cliente como por el servidor del sistema.
 * Su propósito es encapsular la lógica relacionada con el cálculo y la comprobación de hMacs.</p>
 */
public class MessageIntegrityVerifier {
    private static final String[] hexDigits = { "0", "1", "2", "3","4", "5", "6", "7","8", "9", "a", "b","c", "d", "e", "f" };

    /**
     * <p>Lee la información para crear un MessageIntegrityVerifier de un archivo.</p>
     *
     * <p>El formato del archivo es el mismo empleado por el método saveKeyToFile().</p>
     *
     * @param filePath Ruta del archivo con la información de la llave.
     * @return El verificador de integridad inicializado con la llave cargada.
     * @throws IOException Error al acceder al archivo (no encontrado, no se puede abrir o leer).
     */
    public static MessageIntegrityVerifier fromKeyFile(String filePath) throws IOException {
        String key = null;
        String macAlgorithm = null;
        ObjectInputStream inputStream = null;

        try {
            inputStream = new ObjectInputStream(new FileInputStream(filePath));
            key = (String) inputStream.readObject();
            macAlgorithm = (String) inputStream.readObject();
        } catch (ClassNotFoundException cnfe) {
            throw new RuntimeException("Unexpected error: class not found (String?)", cnfe);
        }
        finally {
            if (inputStream != null)
                inputStream.close();
        }

        return new MessageIntegrityVerifier(key, macAlgorithm);
    }

    private SecretKeySpec key;
    private String keyString;
    private String hMacAlgorithm;

    /**
     * Crea un nuevo verificador de integridad de mensajes.
     *
     * @param key Cadena de caracteres con el código de la clave.
     * @param macAlgorithm Algoritmo utilizado. Por ejemplo, "HmacSHA256".
     * @throws UnsupportedEncodingException El algoritmo es incorrecto o no soportado por la plataforma.
     */
    public MessageIntegrityVerifier(String key, String macAlgorithm) throws UnsupportedEncodingException {
        this.hMacAlgorithm = macAlgorithm;
        this.keyString = key;
        this.key = new SecretKeySpec(key.getBytes("UTF-8"), macAlgorithm);
    }

    /**
     * Obtine el mac de un mensaje de texto.
     *
     * @param message Texto del mensaje cuya mac se desea calcular.
     * @return Mac del mensaje en formato hexadecimal.
     * @throws UnsupportedEncodingException La plataforma no soporta UTF-8
     */
    public String getHMac(String message) throws Exception {
        Mac mac = null;
        try {
            mac = Mac.getInstance(hMacAlgorithm);
            mac.init(key);
        } catch (NoSuchAlgorithmException e) {
            throw new Exception("Platform does not support mac algorithm: " + hMacAlgorithm, e);
        } catch (InvalidKeyException e) {
            throw new Exception("Invalid key spec.", e);
        }

        return byteArrayToHexString(mac.doFinal(message.getBytes("UTF-8")));
    }

    /**
     * Comprueba la integridad de un mensaje dado el mismo y su hMac.
     *
     * @param message Mensaje cuya integridad deseamos verificar.
     * @param hMac Código de verificación de integridad.
     * @return True si la integridad del mensaje esta verificada, falso en otro caso.
     * @throws Exception Algoritmo no soportado, codificación UTF-8 no soportada, llave inválida.
     */
    public boolean checkMessage(String message, String hMac) throws Exception {
        String calculatedMac = getHMac(message);
        return calculatedMac.equals(hMac);
    }

    /**
     * <p>Guarda la clave y el algoritmo de cifrado en un archivo.</p>
     *
     * <p>Es necesario tomar las medidas necesarias para proteger dicho archivo, pues contiene
     * toda la información necesaria para construir el verificador de integridad, es decir,
     * el nombre del algoritmo de MAC y la clave utilizada.</p>
     *
     * @param filePath Ruta al archivo donde guardar la información.
     * @throws IOException El archivo no puede ser creado o escrito.
     */
    public void saveKeyToFile(String filePath) throws IOException {
        ObjectOutputStream outputStream = null;
        try {
            outputStream = new ObjectOutputStream(new FileOutputStream(filePath));
            outputStream.writeObject(keyString);
            outputStream.writeObject(hMacAlgorithm);
        } finally {
            if (outputStream != null)
                outputStream.close();
        }
    }

    /**
     * Convierte un array de bytes en una cadena hexadecimal.
     *
     * @param hash Bytes.
     * @return Cadena hexadecimal que representa al array de bytes.
     */
    private static String byteArrayToHexString(byte[] hash) {
        String rval = "";
        for (byte aHash : hash) {
            rval += byteToHexString(aHash);
        }
        return rval;
    }

    /**
     * Convierte un byte a cadena hexadecimal.
     *
     * @param hash Byte
     * @return Cadena hexadecimal que representa el byte.
     */
    private static String byteToHexString(byte hash) {
        int n = hash;
        if (n < 0) {
            n=256 +n;
        }
        int d1 = n / 16;
        int d2 = n % 16;
        return hexDigits[d1] + hexDigits[d2];
    }

    /**
     * <p>Punto de entrada para la utilidad de generación de archivos de clave.</p>
     *
     * <p>Al ejecutar este método main desde la línea de comandos se consigue
     * escribir en el almacenamiento un archivo con los datos de la clave
     * (cadena y algoritmo de hash). Ese archivo puede ser leido por tanto
     * cliente como servidor.</p>
     *
     * @param args Argumentos de entrada.
     * @throws UnsupportedEncodingException
     */
    public static void main(String[] args) throws UnsupportedEncodingException {
        try {
            if (args.length == 1 && args[0].equals("help")) {
                printHelp();
                return;
            }

            KeyFileArgs mainArgs = new KeyFileArgs(args);
            MessageIntegrityVerifier integrityVerifier = new MessageIntegrityVerifier(mainArgs.key, mainArgs.macAlg);

            integrityVerifier.saveKeyToFile(mainArgs.filePath);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    private static void printHelp() {
        System.out.println("INTRed KeyFile Writer Tool");
        System.out.println();
        System.out.println("Usage:");
        System.out.println("  intred-keywriter [-option1=value1 ...]");
        System.out.println("Available options:");
        System.out.println("  -out=<path-to-file>    Path to the key file to be created. By default: keyFile.bin");
        System.out.println("  -key=<key string>      String used as source for the key. By default: defaultKey");
        System.out.println("  -macAlg=<algorithm>    String identifying a supported mac algorithm. By default: HmacSHA256");
        System.out.println();
    }

    private static class KeyFileArgs {
        String filePath;
        String key;
        String macAlg;

        public KeyFileArgs(String[] args) {
            filePath = System.getProperty("user.dir") + File.separator + "keyFile.bin";
            key = "defaultKey";
            macAlg = "HmacSHA256";

            for (String word : args) {
                if (word.startsWith("-out="))
                    filePath = word.replace("-out=", "");
                else if (word.startsWith("-key="))
                    key = word.replace("-key=", "");
                else if (word.startsWith("-macAlg="))
                    macAlg = word.replace("-macAlg=", "");
                else
                    throw new IllegalArgumentException("Unknown argument: " + word);
            }
        }
    }
}
