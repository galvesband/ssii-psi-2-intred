package ssii.psi2.intred;

import javax.net.ServerSocketFactory;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

/**
 * <p>Implementa un servidor TCP con el protocolo de comunicaciones definido por ServerConnection</p>
 *
 * <p>Este es un servidor de demostración, por lo que no implementa soporte para varias conexiones
 * concurrentes.</p>
 *
 * <p>Esta clase implementa la interfaz Runnable, por lo que es posible ejecutarla en un proceso
 * paralelo con facilidad.</p>
 *
 * <p>El servidor imprimirá en un archivo de logs información sobre todos los mensajes recibidos.
 * Para cambiar el archivo de log utilice el método setLogFilePath().</p>
 *
 * @see ServerConnection
 */
public class IntegrityVerifierServer implements Runnable {
    private ServerSocket serverSocket;
    private MessageIntegrityVerifier integrityVerifier;
    private NonceGenerator nonceGenerator;
    private String logFilePath = System.getProperty("user.dir") + File.separator + "server.log";

    /**
     * <p>Crea un nuevo servidor leyendo la clave de un archivo en disco.</p>
     *
     * <p>El servidor no esperará conexiones hasta que no se invoque al método
     * run().</p>
     *
     * @param keyFilePath Ruta al archivo con la clave de integridad. Debe ser el mismo archivo usado por el cliente.
     * @param port Puerto de escucha del servidor.
     * @throws Exception
     */
    public IntegrityVerifierServer(String keyFilePath, int port) throws Exception {
        MessageIntegrityVerifier integrityVerifier = MessageIntegrityVerifier.fromKeyFile(keyFilePath);
        NonceGenerator nonceGenerator = new NonceGenerator();
        init(integrityVerifier, nonceGenerator, port);
    }

    /**
     * <p>Crea un nuevo servidor</p>
     *
     * <p>La clave y el algoritmo para hMac se crearán a partir de los argumentos de entrada.</p>
     *
     * @param key Cadena de texto usada como clave. Debe ser la misma del cliente.
     * @param macAlgorithm Algoritmo para garantizar la integridad. Por ejemplo, 'HmacSHA256'. Debe ser el mismo que el del cliente.
     * @param port Puerto de escucha del servidor.
     * @throws Exception
     */
    public IntegrityVerifierServer(String key, String macAlgorithm, int port) throws Exception {
        MessageIntegrityVerifier integrityVerifier = new MessageIntegrityVerifier(key, macAlgorithm);
        NonceGenerator nonceGenerator = new NonceGenerator();
        init(integrityVerifier, nonceGenerator, port);
    }

    /**
     * <p>Crea un nuevo servidor.</p>
     *
     * <p>El servidor utilizará los argumentos de entrada del método para garantizar la integridad
     * y generar los nonces de la comunicación.</p>
     *
     * @param integrityVerifier Verificador de integridad de mensajes. Debe haber sido inicializado con la misma clave que el cliente.
     * @param nonceGenerator Generador de nonces que se utilizará durante la comunicación.
     * @param port Puerto de escucha.
     * @throws Exception
     */
    public IntegrityVerifierServer(MessageIntegrityVerifier integrityVerifier, NonceGenerator nonceGenerator, int port) throws Exception {
        init(integrityVerifier, nonceGenerator, port);
    }

    private void init(MessageIntegrityVerifier integrityVerifier, NonceGenerator nonceGenerator, int port) throws Exception {
        if (port < 1024 || port > 65535) {
            throw new IllegalArgumentException("Invalid port number: should be in [1024-65535] range.");
        }

        this.integrityVerifier = integrityVerifier;
        this.nonceGenerator = nonceGenerator;

        // Crear un ServerSocket
        ServerSocketFactory socketFactory = ServerSocketFactory.getDefault();
        serverSocket = socketFactory.createServerSocket(port);
    }

    public String getLogFilePath()          { return this.logFilePath; }
    public void setLogFilePath(String path) { this.logFilePath = path; }

    /**
     * <p>Ejecuta el servidor.</p>
     *
     * <p>Este método es bloqueante.</p>
     *
     * <p>Al ejecutarse este método se abrirá el puerto de escucha y se recibirán conexiones. Solo una conexión
     * es posible en cada momento. Esto es por diseño; implementar una versión concurrente del servidor
     * es posible gracias a que esta clase utiliza ServerConnection para procesar las conexiones y esa
     * clase implementa la interfaz Runnable, por lo que debe ser sencillo utilizar Threads o un ThreadPool
     * con las conexiones entrantes. Estas por lo general son independientes unas de otras.</p>
     */
    @Override
    public void run() {
        BufferedWriter logWriter = null;
        try {
            logWriter = new BufferedWriter(
                    new OutputStreamWriter(
                            new FileOutputStream(
                                    new File(logFilePath),
                                    true
                            )
                    )
            );
            System.out.println("Server: log file: " + logFilePath);
        } catch (FileNotFoundException fnfe) {
            System.err.println("Server: Can't create/open log file: " + this.logFilePath);
            return;
        }

        while (true) {
            Socket socket = null;

            try {
                System.out.println("Server: Waiting for client connections...");
                try {
                    socket = serverSocket.accept();
                } catch (SocketException se) {
                    // Socket cerrado
                    break;
                }
                // FIXME Mover todo este bloque dentro del try/catch de arriba?
                ServerConnection connection = new ServerConnection(socket, integrityVerifier, nonceGenerator, logWriter);
                connection.run();
                if (connection.hasError()) {
                    throw connection.getError();
                }
            }
            catch (Exception e) {
                //e.printStackTrace();
                System.err.println("Server: Error while processing a connection: " + e.getMessage());
            }
            finally {
                try {
                    if (socket != null)
                        socket.close();

                    logWriter.close();
                } catch (Exception e) {
                    System.err.println("Server: Error closing server: " + e.getMessage());
                }
            }
        }
    }

    /**
     * <p>Cierra el socket de escucha.</p>
     *
     * <p>Dado que el método run() es bloqueante y el bloqueo depende de la espera de nuevas conexiones
     * (y siendo este bloqueo no interrumpible) la única forma de interrumpir el servidor vía software
     * es llamar a este método desde otro hilo de ejecución. Al cerrarse el socket se produce
     * una SocketException en el hilo que ejecuta el método run().</p>
     *
     * @throws IOException
     */
    public void close() throws IOException {
        if (!serverSocket.isClosed())
            serverSocket.close();
    }

    /**
     * <p>Punto de entrada del proceso servidor.</p>
     *
     * <p>Estos son los posibles argumentos:</p>
     * <ul>
     *     <li>"-key=" seguido de la cadena utiliada como llave. Por defecto es 'defaultKey'.</li>
     *     <li>"-macAlg=" seguido de la cadena que define al algoritmo de hMac. Por defecto es HmacSHA256.</li>
     *     <li>"-keyFile=" seguido de la ruta al archivo con la llave. El archivo incluye la llave y el algoritmo de hMac. Esta opción tiene preferencia sobre '-key' y '-macAlg'.</li>
     *     <li>"-logFile=" seguido de la ruta al archivo de log del servidor.</li>
     *     <li>"-port=" seguido del número del puerto de escucha del servidor. Debe estar entre 1024 y 65535.</li>
     * </ul>
     *
     * @param args Argumentos de entrada.
     * @throws Exception
     */
    public static void main(String args[]) throws Exception {
        try {
            if (args.length == 1 && args[0].equals("help")) {
                printHelp();
                return;
            }

            ServerCommandLineArguments serverArgs = new ServerCommandLineArguments(args);

            IntegrityVerifierServer server;
            if (serverArgs.hasKeyFileOption) {
                System.out.println("Using key file: " + serverArgs.keyFilePath);
                server = new IntegrityVerifierServer(serverArgs.keyFilePath, serverArgs.port);
            }
            else
                server = new IntegrityVerifierServer(serverArgs.keyString, serverArgs.macAlg, serverArgs.port);

            server.setLogFilePath(serverArgs.logFilePath);
            server.run();
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    private static void printHelp() {
        System.out.println("INTRed Server - Integrity Verifier Server");
        System.out.println();
        System.out.println("Usage:");
        System.out.println("  intred-server [-option1=value1 ...]");
        System.out.println("Available options:");
        System.out.println("  -key=<key string>         String used as source for the key. By default: defaultKey");
        System.out.println("  -macAlg=<algorithm>       String identifying a supported mac algorithm. By default: HmacSHA256");
        System.out.println("  -keyFile=<path-to-file>   Path to a key file. If this option is present, it will override key and macAlg. By default: keyFile.bin");
        System.out.println("  -logFile=<path-to-log>    Path to the file used for server logging.");
        System.out.println("  -port=<port number>       Integer between 1024 and 65535. The server will listen in that port. By default: 7070");
        System.out.println();
    }

    private static class ServerCommandLineArguments {
        String keyString = "defaultKey";
        boolean hasKeyFileOption = false;
        String keyFilePath = System.getProperty("user.dir") + File.separator + "keyFile.bin";
        String macAlg = "HmacSHA256";
        String logFilePath = System.getProperty("user.dir") + File.separator + "server.log";
        int port = 7070;

        ServerCommandLineArguments(String [] args) {
            for (String pairKeyValue : args) {
                if (pairKeyValue.startsWith("-key=")) {
                    keyString = pairKeyValue.replace("-key=", "");
                }
                else if (pairKeyValue.startsWith("-keyFile=")) {
                    hasKeyFileOption = true;
                    keyFilePath = pairKeyValue.replace("-keyFile=", "");
                }
                else if (pairKeyValue.equals("-keyFile")) {
                    hasKeyFileOption = true;
                }
                else if (pairKeyValue.startsWith("-macAlg=")) {
                    macAlg = pairKeyValue.replace("-macAlg=", "");
                }
                else if (pairKeyValue.startsWith("-logFile=")) {
                    logFilePath = pairKeyValue.replace("-logFile=", "");
                }
                else if (pairKeyValue.startsWith("-port=")) {
                    try { port = Integer.parseInt(pairKeyValue.replace("-port=", "")); }
                    catch (Exception e) { throw new IllegalArgumentException("Port should be a number: " + pairKeyValue); }
                }
                else {
                    throw new IllegalArgumentException("Unknown argument: " + pairKeyValue);
                }
            }
        }
    }
}
