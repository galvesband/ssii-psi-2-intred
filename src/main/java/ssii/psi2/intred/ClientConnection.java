package ssii.psi2.intred;

import java.io.*;
import java.net.Socket;

/**
 * <p>Implementa el protocolo del cliente.</p>
 *
 * <p>Para ver una descripción del protocolo, vea ServerConnection.</p>
 *
 * @see ServerConnection
 */
public class ClientConnection {
    private static final String QUERY_STRING = "%dayNonce% %serverNonce% %requestNonce% %operation% %message%";

    /**
     * Encapsula los datos de una respuesta del servidor.
     */
    private static class ResponseData {
        /**
         * Primera línea completa de la respuesta (sobre la que se calcula el hMac)
         */
        String message = "";
        /**
         * Indica si la petición ha sido procesada correctamente por el servidor.
         */
        boolean isSuccess = false;
        /**
         * DayNonce incluida en la respuesta. Debe ser igual que el del mensaje de bienvenida.
         */
        int dayNonce = 0;
        /**
         * Nonce incluido en la petición por parte del cliente.
         */
        String requestNonce = "";
        /**
         * Nonce que el servidor espera para la siguiente petición.
         */
        String challengenonce = "";
        /**
         * hMac del mensaje (message)
         */
        String mac = "";
    }

    /**
     * Compone la primera línea de un mensaje al servidor.
     *
     * @param dayNonce Nonce diario del servidor. Se obtiene del mensaje de bienvenida del mismo.
     * @param serverNonce Nonce de un solo uso que el servidor provee para cada petición.
     * @param requestNonce Nonce de un solo uso del cliente. El servidor debe utilizarlo en la respuesta.
     * @param operation Tipo de operación (REQUEST o CLOSE).
     * @param message Mensaje a incluir en la petición.
     * @return Primera línea del mensaje a enviar al servidor.
     */
    private static String composeRequest(int dayNonce, String serverNonce, String requestNonce, ServerConnection.Operation operation, String message) {
        return QUERY_STRING
                .replace("%dayNonce%", String.valueOf(dayNonce))
                .replace("%serverNonce%", serverNonce)
                .replace("%requestNonce%", requestNonce)
                .replace("%operation%", operation.toString())
                .replace("%message%", message);
    }

    private MessageIntegrityVerifier integrityVerifier;
    private NonceGenerator nonceGenerator;
    private Socket socket;
    private int nonceBitSize = 64;
    private BufferedReader input = null;
    private BufferedWriter output = null;

    // Datos de protocolo
    private int dayNonce = 0;
    private String serverNonce = "";

    /**
     * <p>Crea una nueva conexión al servidor conectado al otro lado del socket.</p>
     *
     * <p>Una vez la conexión esta abierta, espera el mensaje de bienvenida del servidor
     * y lo procesa, anotando el dayNonce y el challengeNonce del mismo.</p>
     *
     * @param socket Conexión al servidor.
     * @param integrityVerifier Verificador de integridad de mensajes.
     * @param nonceGenerator Generador de nonces.
     * @throws Exception Ante errores de comunicación.
     */
    public ClientConnection(Socket socket, MessageIntegrityVerifier integrityVerifier, NonceGenerator nonceGenerator) throws Exception {
        this.integrityVerifier = integrityVerifier;
        this.nonceGenerator = nonceGenerator;
        this.socket = socket;

        try {
            input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            output = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));

            // Leer mensaje de bienvenida
            String welcomeMessage = input.readLine();
            if (welcomeMessage == null)
                throw new EOFException("Server closed connection unexpectedly.");
            String mac = input.readLine();
            if (mac == null)
                throw new EOFException("Server closed connection unexpectedly.");

            // Comprobar integridad
            if (!integrityVerifier.checkMessage(welcomeMessage, mac))
                throw new IntredException(IntredException.ErrorCode.INTEGRITY_ERROR);

            // Capturar dayNonce y serverNonce
            parseWelcomeMessage(welcomeMessage);
        } catch (Exception e) {
            // Ante cualquier error intentar cerrar la conexión de forma controlada
            if (input != null) {
                input.close();
                input = null;
            }
            if (output != null) {
                output.close();
                output = null;
            }
            if (socket != null && socket.isConnected())
                socket.close();

            // Y relanzar la excepción
            throw e;
        }
    }

    private void parseWelcomeMessage(String message) throws Exception {
        System.out.println("Client: Server said: " + message);

        String [] messageWords = message.split(" ");
        if (messageWords.length != 4)
            throw new IntredException(IntredException.ErrorCode.PROTOCOL_ERROR);
        if (!messageWords[0].equals("200") || !messageWords[1].equals("HELLO"))
            throw new IntredException(IntredException.ErrorCode.PROTOCOL_ERROR);

        try { this.dayNonce = Integer.parseInt(messageWords[2]); }
        catch (Exception e) { throw new IntredException(IntredException.ErrorCode.PROTOCOL_ERROR); }
        this.serverNonce = messageWords[3];
    }

    /**
     * <p>Hace una petición al servidor.</p>
     *
     * <p>La petición enviada al servidor será de tipo REQUEST y utilizará el
     * dayNonce y requestNonce indicados por el servidor en su mensaje de
     * bienvenida o respuesta a una petición anterior.</p>
     *
     * <p>La petición incluirá un nonce generado por el cliente y la respuesta
     * del servidor debe incluirlo.</p>
     *
     * <p>El método espera a recibir la respuesta del servidor y comprueba su
     * validez (integridad, dayNonce, requestNonce) además de anotar el
     * challengeNonce para la siguiente petición.</p>
     *
     * @param message Petición que realizar al servidor.
     * @return Cierto si la petición ha tenido éxito, falso en otro caso.
     * @throws Exception Si hay errores en la comunicación
     */
    public boolean sendRequestOperation(String message) throws Exception {
        if (input == null || output == null || socket == null || socket.isClosed())
            throw new RuntimeException("Error attempting to send a request: connection is closed.");

        try {
            ServerConnection.RequestData request = createRequest(
                    dayNonce,
                    serverNonce,
                    nonceGenerator.getNextNonceString(nonceBitSize),
                    ServerConnection.Operation.REQUEST,
                    message
            );
            output.write(request.message + "\n");
            output.write(request.mac + "\n");
            output.flush();

            // Receive response
            ResponseData response = receiveResponse(input);
            // Validar integridad
            if (!integrityVerifier.checkMessage(response.message, response.mac))
                throw new IntredException(IntredException.ErrorCode.INTEGRITY_ERROR);
            // Comprobar nonces
            if (response.dayNonce != request.dayNonce)
                throw new IntredException(IntredException.ErrorCode.NONCE_ERROR);
            if (!response.requestNonce.equals(request.requestNonce))
                throw new IntredException(IntredException.ErrorCode.NONCE_ERROR);
            // Anotar el nonce del servidor para la siguiente petición
            serverNonce = response.challengenonce;
            // Devolver true o false según respuesta
            return response.isSuccess;
        } catch (Exception e) {
            try {
                close();
            } catch (Exception e2) {
                // e2 se produce al intentar cerrar la conexión durante el manejo
                // de otra excepción. Vamos a darle preferencia a e.
                // Noop.
            }
            throw e;
        }
    }

    /**
     * <p>Cierra la conexión con el servidor.</p>
     *
     * <p>Envia una petición de cierre de conexión al servidor y espera a que este
     * cierre la conexión.</p>
     *
     * <p>En cualquier caso, después de llamar a este método la conexión estará cerrada
     * y no se podrán hacer más peticiones.</p>
     *
     * @throws Exception Si ocurre algún error durante el cierre de la conexión.
     */
    public void close() throws Exception {
        try {
            if (input != null && output != null && socket != null && !socket.isClosed()) {
                // Si conexión abierta, mandar un CLOSE
                String data = composeRequest(
                        dayNonce,
                        serverNonce,
                        nonceGenerator.getNextNonceString(nonceBitSize),
                        ServerConnection.Operation.CLOSE,
                        ""
                );
                output.write(data + "\n");
                output.write(integrityVerifier.getHMac(data) + "\n");
                output.flush();

                // Esperar a que servidor cierre la conexión
                String tmp;
                if ((tmp = input.readLine()) != null)
                    System.err.println("Server did not close connection when expected and said: " + tmp);
            }
        } finally {
            if (input != null) {
                input.close();
                input = null;
            }
            if (output != null) {
                output.close();
                output = null;
            }
            if (socket != null) {
                if (!socket.isClosed()) socket.close();
                socket = null;
            }
        }
    }

    private ServerConnection.RequestData createRequest(int dayNonce, String serverNonce, String requestNonce, ServerConnection.Operation operation, String request) throws Exception {
        ServerConnection.RequestData rval = new ServerConnection.RequestData();
        rval.dayNonce = dayNonce;
        rval.challengeNonce = serverNonce;
        rval.requestNonce = requestNonce;
        rval.request = request;
        rval.message = composeRequest(dayNonce, serverNonce, requestNonce, operation, request);
        rval.mac = integrityVerifier.getHMac(rval.message);

        return rval;
    }

    private ResponseData receiveResponse(BufferedReader input) throws IOException, IntredException {
        ResponseData rval = new ResponseData();
        rval.message = input.readLine();
        if (rval.message == null)
            throw new EOFException("Server closed connection unexpectedly.");

        rval.mac = input.readLine();
        if (rval.mac == null)
            throw new EOFException("Server closed connection unexpectedly.");

        String[] firstLineWords = rval.message.split(" ");
        if (firstLineWords.length != 4)
            throw new IntredException(IntredException.ErrorCode.PROTOCOL_ERROR);

        // Primer campo es el código de respuesta
        switch (firstLineWords[0]) {
            case "200":
                rval.isSuccess = true;
                break;
            case "500":
                rval.isSuccess = false;
                break;
            default:
                throw new IntredException(IntredException.ErrorCode.PROTOCOL_ERROR);
        }
        //"%CODE% %dayNonce% %clientNonce% %serverNonce%"
        // Segundo campo es el dayNonce
        try { rval.dayNonce = Integer.parseInt(firstLineWords[1]); }
        catch (Exception e) { throw new IntredException(IntredException.ErrorCode.PROTOCOL_ERROR); }

        // Tercer campo es el clientNonce
        rval.requestNonce = firstLineWords[2];

        // Cuarto campo es el challengeNonce para la siguiente petición
        rval.challengenonce = firstLineWords[3];

        return rval;
    }

    /**
     * <p>Devuelve el número de bits que tienen los nonces generados por el cliente.</p>
     *
     * @return Número de bits.
     */
    public int getNonceBitSize() {
        return nonceBitSize;
    }

    /**
     * <p>Establece el número de bits con los que se generarán los nonces del cliente.</p>
     *
     * <p>Debe ser divisible por 8.</p>
     *
     * @param nonceBitSize Nuevo número de bits de los nonces del cliente. Debe ser divisible por 8.
     */
    public void setNonceBitSize(int nonceBitSize) {
        if (nonceBitSize % 8 != 0)
            throw new IllegalArgumentException("'nonceBitSize' should be divisible by 8.");

        this.nonceBitSize = nonceBitSize;
    }
}
