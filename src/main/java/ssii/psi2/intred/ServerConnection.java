package ssii.psi2.intred;

import java.io.*;
import java.net.Socket;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

/**
 * <p>Representa una conexión del servidor.</p>
 *
 * <p>Maneja una conexión del servidor con el cliente, validando
 * la integridad de la comunicación y manteniendo un control de nonces
 * entre el cliente y el servidor.</p>
 *
 * <p>El protocolo de comunicación es muy simple:</p>
 *
 * <pre>
 *     * Cliente abre conexión
 *     * Servidor recibe apertura y responde con mensaje de bienvenida:
 *         '200 HELLO $dayNonce$ $challengeNonce$'
 *         '$mac de la línea anterior$'
 *     * En este punto entramos en el bucle de peticiones de la conexión
 *     * Cliente envía petición con el siguiente formato:
 *         '$dayNonce$ $challengeNonce$ $requestNonce$ $operación$ $datos de petición$
 *         '$mac de la línea anterior$'
 *       La operación puede ser REQUEST o CLOSE.
 *       dayNonce debe ser el mismo que le envió el servidor en el mensaje inicial.
 *       challenegeNonce debe ser el nonce que envió el servidor en su mensaje.
 *       requestNonce lo elige el cliente, es el nónce con el que el servidor debe responder.
 *     * Si la operación es REQUEST el servidor responderá con:
 *         '$código de resultado$ $dayNonce$ $requestNonce$ $challengeNonce$'
 *         '$mac de la línea anterior$'
 *       El código de resultado puede ser 200 para éxito o 500 si es un error.
 *       dayNonce debe ser el mismo que el servidor envió en el mensaje inicial.
 *       requestNonce debe ser el nonce que envió el cliente en su petición.
 *       challengeNonce es un nuevo nonce del servidor para la siguiente petición.
 *       El cliente podrá hacer otra petición a continuación.
 *     * Si la operación es CLOSE el servidor cerrará la conexión.
 * </pre>
 *
 * <p>Toda petición recibida del cliente se traduce en una línea en el log del servidor.</p>
 *
 * <p>Esta clase implementa Runnable, por lo que puede ser integrada
 * con facilidad en ThreadPools o Threads.</p>
 */
public class ServerConnection implements Runnable {
    private final String HELLO_STRING = "200 HELLO %dayNonce% %initialNonce%";
    private final String RESPONSE_STRING = "%CODE% %dayNonce% %clientNonce% %serverNonce%";
    private final String SUCCESS_REQUEST_LOG_STRING = "OK %ip% %timestamp% %result% -- %request%";
    private final String FAILURE_REQUEST_LOG_STRING = "ER %ip% %timestamp% %error_code% -- %request%";

    /**
     * Tipos de operaciones del protocolo que puede enviar el cliente.
     */
    public enum Operation {
        /**
         * El cliente quiere terminar la conexión.
         */
        CLOSE,
        /**
         * El cliente quiere hacer una petición (y los datos de la misma deben ir en ese mensaje).
         */
        REQUEST
    }

    /**
     * Encapsula los datos de una petición recibida por el servidor.
     */
    public static class RequestData {
        /**
         * Primera línea completa de la petición (sobre la que se calcula el hMac)
         */
        String message = "";
        /**
         * Debe ser igual que el dayNonce enviado por el servidor en su mensaje de saludo.
         */
        int dayNonce = 0;
        /**
         * Nonce que el servidor envió al cliente y con la que este responde.
         */
        String challengeNonce = "";
        /**
         * Nonce que el cliente nos envía.
         */
        String requestNonce = "";
        /**
         * Operación que representa la petición.
         */
        Operation operation = Operation.CLOSE;
        /**
         * Si hay una petición (operation es REQUEST), estos son los datos de la misma.
         */
        String request = "";
        /**
         * hMac del mensaje (message).
         */
        String mac;
    }

    private MessageIntegrityVerifier integrityVerifier;
    private NonceGenerator nonceGenerator;
    private Socket connection;
    private BufferedWriter log;
    private int nonceBitSize = 64;
    private Exception exception = null;

    /**
     * <p>Crea una nueva conexión con el cliente.</p>
     *
     * <p>La conexión no empieza a ser procesada
     * hasta que no se llame al método run().</p>
     *
     * @param connection Socket de la conexión recibida.
     * @param integrityVerifier Verificador de integridad de mensajes.
     * @param nonceGenerator Generador de nonces.
     * @param log En este objeto se escribirán las líneas de log.
     */
    public ServerConnection (Socket connection, MessageIntegrityVerifier integrityVerifier, NonceGenerator nonceGenerator, BufferedWriter log) {
        this.connection = connection;
        this.integrityVerifier = integrityVerifier;
        this.nonceGenerator = nonceGenerator;
        this.log = log;
    }

    /**
     * <p>Devuelve cierto si ha habido un error durante el proceso de la conexión.</p>
     *
     * <p>Se puede llamar a getError() para obtener la excepción que
     * se produjo durante el procesado de la conexión.</p>
     *
     * @return cierto si hay un error.
     */
    boolean hasError() {
        return exception != null;
    }

    /**
     * <p>Devuelve la excepción ocurrida durante la conexión o null.</p>
     *
     * <p>Para saber si ha habido un error durante el procesado de la conexión
     * llame a getError().</p>
     *
     * @return La excepción que produjo un error o null.
     */
    Exception getError() {
        return exception;
    }

    /**
     * Número de bits de los nonces generados por el servidor. Por defecto 64.
     *
     * @return Número de bits de los nonces.
     */
    public int getNonceBitSize() {
        return nonceBitSize;
    }

    /**
     * <p>Establece el número de bits de los nonces del servidor.</p>
     *
     * <p>Por defecto vale 64. El nuevo valor debe ser divisible entre 8.</p>
     *
     * @param nonceBitSize Nuevo número de bits. Debe ser divisible entre 8.
     */
    public void setNonceBitSize(int nonceBitSize) {
        if (nonceBitSize % 8 != 0) {
            throw new IllegalArgumentException("'nonceBitSize' should be divisible by 8.");
        }
        this.nonceBitSize = nonceBitSize;
    }

    /**
     * <p>Procesa la conexión.</p>
     *
     * <p>Si se produce una excepción, el método terminará normalmente. Es
     * responsabilidad de la clase usuaria llamar a hasError() y en caso
     * de que devuelva <i>true</i>, getError().</p>
     */
    @Override
    public void run() {
        BufferedReader input = null;
        BufferedWriter output = null;
        // dayNonce es el número de días desde EPOCH (del orden de 17000)
        // El dayNonce no cambia durante una conexión.
        int dayNonce = (int) ChronoUnit.DAYS.between(LocalDate.ofEpochDay(0), LocalDate.now());

        try {
            input = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            output = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream()));

            // Enviar HELLO
            String challengeNonce = nonceGenerator.getNextNonceString(nonceBitSize);
            sendInitialMessage(challengeNonce, dayNonce, output);

            boolean isClosing = false;
            while (!isClosing) {
                // Recibimos la petición
                RequestData request = receiveRequest(input);
                // Comprobar integridad
                if (!integrityVerifier.checkMessage(request.message, request.mac))
                    throw new IntredException(IntredException.ErrorCode.INTEGRITY_ERROR, request);
                // Comprobar dayNonce
                if (request.dayNonce != dayNonce)
                    throw new IntredException(IntredException.ErrorCode.NONCE_ERROR, request);
                // Comprobar challengeNonce
                if (!request.challengeNonce.equals(challengeNonce))
                    throw new IntredException(IntredException.ErrorCode.NONCE_ERROR, request);
                // En este punto la petición es íntegra y el nonce concuerda

                // Procesar petición
                boolean isSuccess = false;
                switch (request.operation) {
                    case CLOSE:
                        isClosing = true;
                        isSuccess = true;
                        break;
                    case REQUEST:
                        // Procesar petición
                        isSuccess = processRequest(request);
                        challengeNonce = nonceGenerator.getNextNonceString(nonceBitSize);
                        sendResponse(isSuccess, dayNonce, challengeNonce, request.requestNonce, output);
                        break;
                    default:
                        throw new IntredException(IntredException.ErrorCode.UNKNOWN_OPERATION, request);
                }

                // Escribir en el log la petición correcta
                logSuccessRequest(request, isSuccess);
            }
        } catch (IntredException ie) {
            // Escribir en el log el error
            try { logFailureRequest(ie); }
            catch (IOException e) { System.err.println("Error writing an error to log file!"); }

            // Tras la conexión, el usuario de esta clase debe comprobar si hay
            // un error (hasError()) y en tal caso llamar a getError() para
            // obtener la excepción y controlarla.
            exception = ie;
        } catch (EOFException eofe) {
            try { logFailureRequest(eofe); }
            catch (IOException e) { System.err.println("Error writing an error to log file!"); }
        }
        catch (Exception e) {
            exception = e;
        }
        finally {
            try {
                if (input != null) input.close();
                if (output != null) output.close();
                if (connection != null) connection.close();
            } catch (IOException e) {
                System.err.println("Error closing input stream, output stream or socket with client:");
                System.err.println(e.getMessage());
            }
        }
    }

    private void sendInitialMessage(String initialNonce, int dayNonce, BufferedWriter output) throws Exception {
        String data = HELLO_STRING
                .replace("%initialNonce%", initialNonce)
                .replace("%dayNonce%", String.valueOf(dayNonce));
        String mac = integrityVerifier.getHMac(data);
        output.write(data + "\n" + mac + "\n");
        output.flush();
    }

    private RequestData receiveRequest(BufferedReader input) throws IOException, IntredException {
        RequestData rval = new RequestData();
        rval.message = input.readLine();
        if (rval.message == null)
            throw new EOFException("Client closed connection unexpectedly.");

        rval.mac = input.readLine();
        if (rval.mac == null)
            throw new EOFException("Client closed connection unexpectedly.");

        String[] firstLineWords = rval.message.split(" ");
        if (firstLineWords.length < 4)
            throw new IntredException(IntredException.ErrorCode.PROTOCOL_ERROR);

        // Primer campo debe el dayNonce
        try { rval.dayNonce = Integer.parseInt(firstLineWords[0]); }
        catch (Exception e) { throw new IntredException(IntredException.ErrorCode.PROTOCOL_ERROR); }

        // Siguiente campo es el nonce que el servidor le pasó al cliente antes de la petición
        rval.challengeNonce = firstLineWords[1];
        // Este es el nonce que nos envía el cliente. Debemos usarlo al responderle.
        rval.requestNonce = firstLineWords[2];
        // Leemos el tipo de petición.
        switch (firstLineWords[3]) {
            // Una petición para lo que sea que el cliente haga (transacciones bancarias, etc)
            case "REQUEST":
                rval.operation = Operation.REQUEST;
                break;
            // Cliente quiere cerrar la conexión
            case "CLOSE":
                rval.operation = Operation.CLOSE;
                break;
            default:
                throw new IntredException(IntredException.ErrorCode.UNKNOWN_OPERATION);
        }

        // El resto de la primera línea son datos de la petición (la auténtica petición)
        if (firstLineWords.length > 4) {
            rval.request = firstLineWords[4];
            for (int i = 5; i < firstLineWords.length; i++)
                rval.request += " " + firstLineWords[i];
        }

        System.out.println("Server: Client request: " + rval.message);
        return rval;
    }

    /**
     * <p>Ejecuta una petición recibida desde el cliente.</p>
     *
     * <p>Este método es invocado por el servidor cuando la validez de la petición
     * ha sido comprobada, es decir, la integridad de la petición ha sido comprobada y
     * contiene los nonces necesarios.</p>
     *
     * <p>La implementación base de este método se limita a imprimir en la consola
     * los datos de la petición. El cliente de la empresa puede refinar esta clase
     * y reimplementar este método con el código necesario para parsear y procesar
     * la petición del cliente dependiendo de sus necesidades.</p>
     *
     * @param request Datos de la petición del cliente.
     * @return Cierto si la petición se ha procesado correctamente, falso en cualquier otro caso.
     */
    protected boolean processRequest(RequestData request) {
        // En este punto la petición ha sido validada y el nonce comprobado.
        // Lo que queda es procesar la petición (realizar una transferencia bancaria
        // o alguna otra operación del cliente).
        // TODO
        // Imprimimos la petición en el log.
        System.out.println("Server: Valid request processed: " + request.request);
        return true;
    }

    private void sendResponse(boolean isSuccess, int dayNonce, String challengeNonce, String clientNonce, BufferedWriter output) throws Exception {
        String data = RESPONSE_STRING
                .replace("%CODE%", isSuccess ? "200" : "500")
                .replace("%dayNonce%", String.valueOf(dayNonce))
                .replace("%clientNonce%", clientNonce)
                .replace("%serverNonce%", challengeNonce);
        String mac = integrityVerifier.getHMac(data);
        output.write(data + "\n" + mac + "\n");
        output.flush();
    }

    protected void logSuccessRequest(RequestData request, boolean result) throws IOException {
        String logMessage = SUCCESS_REQUEST_LOG_STRING
                .replace("%ip%", connection.getInetAddress().getHostAddress())
                .replace("%timestamp%", LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME))
                .replace("%result%", result ? "SUCCESS" : "FAIL")
                .replace("%request%", request.message);

        log.write(logMessage + "\n");
        log.flush();
    }

    protected void logFailureRequest(IntredException ie) throws IOException {
        String logMessage = FAILURE_REQUEST_LOG_STRING
                .replace("%ip%",         connection.getInetAddress().getHostAddress())
                .replace("%timestamp%",  LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME))
                .replace("%error_code%", ie.getErrorCode().toString())
                .replace("%request%",    ie.hasRequestData() ? ie.getRequestData().message : "request-unknown");

        log.write(logMessage + "\n");
        log.flush();
    }

    protected void logFailureRequest(Exception e) throws IOException {
        String logMessage = FAILURE_REQUEST_LOG_STRING
                .replace("%ip%",         connection.getInetAddress().getHostAddress())
                .replace("%timestamp%",  LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME))
                .replace("%error_code%", "UNKNOWN")
                .replace("%request%",    e.getMessage());

        log.write(logMessage + "\n");
        log.flush();
    }
}
