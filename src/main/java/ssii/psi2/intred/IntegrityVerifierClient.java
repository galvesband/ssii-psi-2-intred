package ssii.psi2.intred;

import javax.net.SocketFactory;
import java.io.*;

/**
 * <p>Implementa un cliente simple para IntegrityVerifierServer o ServerConnection.</p>
 *
 * <p>La implementación de esta clase es un wrapper simple alrededor de ClientConnection.</p>
 */
public class IntegrityVerifierClient {
    private String hostname;
    private int port;
    private ClientConnection connection = null;

    /**
     * <p>Crea un nuevo IntegrityVerifierClient.</p>
     *
     * <p>Durante el constructor la conexión con el servidor es iniciada y el cliente
     * recibe el mensaje de bienvenida del servidor.</p>
     *
     * <p>Este constructor crea su verificador de mensajes basándose en un archivo.</p>
     *
     * @param keyFilePath Path al archivo con la clave y el algoritmo de hMac.
     * @param hostname Nombre del servidor.
     * @param port Puerto del servidor.
     * @throws Exception
     */
    public IntegrityVerifierClient(String keyFilePath, String hostname, int port) throws Exception {
        MessageIntegrityVerifier integrityVerifier = MessageIntegrityVerifier.fromKeyFile(keyFilePath);
        NonceGenerator nonceGenerator = new NonceGenerator();

        init(integrityVerifier, nonceGenerator, hostname, port);
    }

    /**
     * <p>Crea un nuevo IntegrityVerifierClient.</p>
     *
     * <p>Durante el constructor la conexión con el servidor es iniciada y el cliente
     * recibe el mensaje de bienvenida del servidor.</p>
     *
     * <p>Este constructor crea su verificador de mensajes basándose en la cadena de
     * entrada key y en el algoritmo de hMac indicado.</p>
     *
     * @param key Cadena de caracteres a partir de la cual construir la clave.
     * @param macAlg Algoritmo de hMac que se va a utilizar.
     * @param hostname Nombre del servidor.
     * @param port Puerto del servidor.
     * @throws Exception
     */
    public IntegrityVerifierClient(String key, String macAlg, String hostname, int port) throws Exception {
        MessageIntegrityVerifier integrityVerifier = new MessageIntegrityVerifier(key, macAlg);
        NonceGenerator nonceGenerator = new NonceGenerator();

        init(integrityVerifier, nonceGenerator, hostname, port);
    }

    /**
     * <p>Crea un nuevo IntegrityVerifierClient</p>
     *
     * <p>Durante el constructor la conexión con el servidor es iniciada y el cliente
     * recibe el mensaje de bienvenida del servidor.</p>
     *
     * <p>Este constructor utiliza el verificador de mensajes y el generador de nonces
     * recibidos como argumentos.</p>
     *
     * @param integrityVerifier Verificador de integridad de mensajes que el cliente debe usar.
     * @param nonceGenerator Generador de nonces que el cliente debe usar.
     * @param hostname Nombre del servidor.
     * @param port Puerto del servidor.
     * @throws Exception
     */
    public IntegrityVerifierClient(MessageIntegrityVerifier integrityVerifier, NonceGenerator nonceGenerator, String hostname, int port) throws Exception {
        init(integrityVerifier, nonceGenerator, hostname, port);
    }

    private void init(MessageIntegrityVerifier integrityVerifier, NonceGenerator nonceGenerator, String hostname, int port) throws Exception {
        if (port < 1024 || port > 65535)
            throw new IllegalArgumentException("Port should be in the [1024-65535] range.");

        connection = new ClientConnection(
                SocketFactory.getDefault().createSocket(hostname, port),
                integrityVerifier,
                nonceGenerator
        );
    }

    /**
     * Envia una petición al servidor.
     *
     * @param message Cadena que el servidor interpretará como REQUEST.
     * @return True si la operación en el servidor tuvo éxito, falso en otro caso.
     * @throws Exception Si hay cualquier problema de entrada/salida, integridad, nonces, ...
     */
    public boolean doRequest(String message) throws Exception {
        return connection.sendRequestOperation(message);
    }

    /**
     * <p>Cierra la conexión con el servidor.</p>
     *
     * @throws Exception
     */
    public void close() throws Exception {
        connection.close();
    }

    /**
     * Punto de entrada del cliente en línea de comandos.
     *
     * @param args Argumentos de entrada.
     */
    public static void main(String[] args) {
        try {
            if (args.length == 1 && args[0].equals("help")) {
                printHelp();
                return;
            }

            ClientCommandLineArguments clientArgs = new ClientCommandLineArguments(args);

            System.out.println("Connecting to server " + clientArgs.serverHost + ":" + clientArgs.port + "...");
            IntegrityVerifierClient client;
            if (clientArgs.hasKeyFileOption)
                client = new IntegrityVerifierClient(clientArgs.keyFilePath, clientArgs.serverHost, clientArgs.port);
            else
                client = new IntegrityVerifierClient(clientArgs.keyString, clientArgs.macAlg, clientArgs.serverHost, clientArgs.port);

            System.out.println("Sending request: " + clientArgs.request);
            boolean result = client.doRequest(clientArgs.request);
            System.out.println("Request result: " + (result ? "ok" : "fail"));

            System.out.println("Closing connection...");
            client.close();
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    private static void printHelp() {
        System.out.println("INTRed Client - Integrity Verifier Client");
        System.out.println();
        System.out.println("Usage:");
        System.out.println("  intred-client [-option1=value1 ...] <request>");
        System.out.println("Available options:");
        System.out.println("  -key=<key-string>         String used as source for the key. By default: defaultKey");
        System.out.println("  -macAlg=<algorithm>       String identifying a supported mac algorithm. By default: HmacSHA256");
        System.out.println("  -keyFile=<path-to-file>   Path to a key file. If this option is present, it will override key and macAlg. By default: keyFile.bin");
        System.out.println("  -server=<server-address>  IPv4 address or hostname of the server.");
        System.out.println("  -port=<port number>       Integer between 1024 and 65535, port where the server is listening. By default: 7070");
        System.out.println();
        System.out.println("The request will be everything after the last option.");
    }

    private static class ClientCommandLineArguments {
        String keyString = "defaultKey";
        String macAlg = "HmacSHA256";
        boolean hasKeyFileOption = false;
        String keyFilePath = System.getProperty("user.dir") + File.separator + "keyFile.bin";
        String serverHost = "localhost";
        int port = 7070;

        String request = "default request";

        ClientCommandLineArguments(String [] args) {
            boolean inOptions = true;
            for (String word : args) {
                if (inOptions) {
                    if (word.startsWith("-key=")) {
                        keyString = word.replace("-key=", "");
                    }
                    else if (word.startsWith("-macAlg=")) {
                        macAlg = word.replace("-macAlg=", "");
                    }
                    else if (word.startsWith("-keyFile=")) {
                        hasKeyFileOption = true;
                        keyFilePath = word.replace("-keyFile=", "");
                    }
                    else if (word.equals("-keyFile")) {
                        hasKeyFileOption = true;
                    }
                    else if (word.startsWith("-server=")) {
                        serverHost = word.replace("-server=", "");
                    }
                    else if (word.startsWith("-port=")) {
                        try { port = Integer.parseInt(word.replace("-port=", "")); }
                        catch (Exception e) { throw new IllegalArgumentException("Port should be a number: " + word); }
                    }
                    else if (word.startsWith("-")) {
                        throw new IllegalArgumentException("Unknown option: " + word);
                    }
                    else {
                        inOptions = false;
                        request = word;
                    }
                }
                else {
                    request += word;
                }
            }
        }
    }
}
