package ssii.psi2.intred;

import org.junit.Test;
import static org.junit.Assert.*;
import java.security.NoSuchAlgorithmException;

/**
 * Comprueba que NonceGenerator se comporta como debe.
 */
public class NonceGeneratorTest {

    @Test
    public void testConvertBytesToInt() throws Exception {
        byte [] test1 = {0, 0 ,0 ,0};
        assertEquals(0, NonceGenerator.convertBytesToInt(test1));

        byte [] test20 = {0, 0, 0, 1};
        assertEquals(1, NonceGenerator.convertBytesToInt(test20));
        byte [] test21 = {0, 0, 0, -1};
        assertEquals(128, NonceGenerator.convertBytesToInt(test21));
        byte [] test22 = {0, 0, 0, 127};
        assertEquals(127, NonceGenerator.convertBytesToInt(test22));
        byte [] test23 = {0, 0, 0, -128};
        assertEquals(255, NonceGenerator.convertBytesToInt(test23));

        byte [] test30 = {0, 0, 1, 0};
        assertEquals(256, NonceGenerator.convertBytesToInt(test30));
        byte [] test31 = {0, 0, -1, 0};
        assertEquals(32768, NonceGenerator.convertBytesToInt(test31));
        byte [] test32 = {0, 0, 127, 0};
        assertEquals(32512, NonceGenerator.convertBytesToInt(test32));
        byte [] test33 = {0, 0, -128, 0};
        assertEquals(65280, NonceGenerator.convertBytesToInt(test33));

        byte [] test40 = {0, 1, 0, 0};
        assertEquals(65536, NonceGenerator.convertBytesToInt(test40));
        byte [] test41 = {0, -1, 0, 0};
        assertEquals(8388608, NonceGenerator.convertBytesToInt(test41));
        byte [] test42 = {0, 127, 0, 0};
        assertEquals(8323072, NonceGenerator.convertBytesToInt(test42));
        byte [] test43 = {0, -128, 0, 0};
        assertEquals(16711680, NonceGenerator.convertBytesToInt(test43));

        byte [] test50 = {1, 0, 0, 0};
        assertEquals(16777216, NonceGenerator.convertBytesToInt(test50));
        byte [] test51 = {-1, 0, 0, 0};
        assertEquals(-16777216, NonceGenerator.convertBytesToInt(test51));
        byte [] test52 = {127, 0, 0, 0};
        assertEquals(2130706432, NonceGenerator.convertBytesToInt(test52));
        byte [] test53 = {-128, 0, 0, 0};
        assertEquals(-2147483648, NonceGenerator.convertBytesToInt(test53));

        assertEquals(Integer.MAX_VALUE, NonceGenerator.convertBytesToInt(new byte[] {127, -128, -128, -128}));
        assertEquals(Integer.MIN_VALUE, NonceGenerator.convertBytesToInt(new byte[] {-128, 0, 0, 0}));
    }

    @Test
    public void testConvertBytesToLong() throws Exception {
        byte [] test1 = {0, 0 ,0 ,0, 0, 0, 0, 0};
        assertEquals(0, NonceGenerator.convertBytesToLong(test1));
        assertEquals(Long.MAX_VALUE, NonceGenerator.convertBytesToLong(new byte[] {127, -128, -128, -128, -128, -128, -128, -128}));
        assertEquals(Long.MIN_VALUE, NonceGenerator.convertBytesToLong(new byte[] {-128, 0, 0, 0, 0, 0, 0, 0}));
    }

    @Test
    public void testNextNonce32() throws NoSuchAlgorithmException {
        NonceGenerator generator = new NonceGenerator();

        int val1 = generator.getNextNonce32();
        assertNotEquals(val1, generator.getNextNonce32());
    }

    @Test
    public void testNextNonce64() throws NoSuchAlgorithmException {
        NonceGenerator generator = new NonceGenerator();

        long val1 = generator.getNextNonce64();
        assertNotEquals(val1, generator.getNextNonce64());
    }

    @Test
    public void testNextNonceString() throws Exception {
        NonceGenerator generator = new NonceGenerator();

        String val1 = generator.getNextNonceString(8);
        assertEquals(2, val1.length());
        String val2 = generator.getNextNonceString(256);
        assertEquals(64, val2.length());
        String val3 = generator.getNextNonceString(256);
        assertNotEquals(val2, val3);
    }
}
