package ssii.psi2.intred;

import org.junit.Test;

import java.io.File;

import static org.junit.Assert.*;

/**
 * Comprueba que MessageIntegrityVerifier funciona como debe.
 */
public class MessageIntegrityVerifierTest {
    @Test
    public void testOwnMessage() throws Exception {
        MessageIntegrityVerifier verifier = new MessageIntegrityVerifier("MyKey", "HmacSHA256");

        String message = "hello world";
        String messageMac = verifier.getHMac(message);
        assertTrue(verifier.checkMessage(message, messageMac));
    }

    @Test
    public void testValidMessage() throws Exception {
        MessageIntegrityVerifier verifier = new MessageIntegrityVerifier("qnscAdgRlkIhAUPY44oiexBKtQbGY0orf7OV1I50", "HmacSHA1");
        String message = "foo";

        // Calculado con una herramienta externa
        String mac = "fb78768298dfe317329e308653995b74c07018e7";
        assertTrue(verifier.checkMessage(message, mac));
    }

    @Test
    public void testInvalidMessage() throws Exception {
        // En este caso estamos usando la misma llave y algoritmo que en el test válido (testValidMessage).
        // Pero el mensaje es distinto, por lo que la comprobación debe fallar.
        MessageIntegrityVerifier verifier = new MessageIntegrityVerifier("qnscAdgRlkIhAUPY44oiexBKtQbGY0orf7OV1I50", "HmacSHA1");
        String message = "hello world!";

        assertFalse(verifier.checkMessage(message, "fb78768298dfe317329e308653995b74c07018e7"));
    }

    @Test
    public void testLoadKey() throws Exception {
        // Comprobamos que un verificador es capaz de leer una llave y validar con éxito un mensaje
        // cuya mac creó el verificador que escribió la llave.
        String filePath = System.getProperty("user.dir") + File.separator + "key.bin";
        String mac1;
        String message = "Hello World";
        try {
            {
                // Verificador que escribe la llave y calcula el mac del mensaje.
                MessageIntegrityVerifier verifier1 = new MessageIntegrityVerifier("MyKey", "HmacSHA256");
                verifier1.saveKeyToFile(filePath);
                mac1 = verifier1.getHMac(message);
            }

            {
                // Verificador que lee la llave y reconoce el mensaje como integro
                // con la mac que generó el primer verificador.
                MessageIntegrityVerifier verifier2 = MessageIntegrityVerifier.fromKeyFile(filePath);
                assertTrue(verifier2.checkMessage(message, mac1));
            }
        } finally {
            // Si la llave llegó a escribirse en disco, borrarla al finalizar el test.
            File keyFile = new File(filePath);
            if (keyFile.exists()) {
                assertTrue(keyFile.delete());
            }
        }
    }
}
