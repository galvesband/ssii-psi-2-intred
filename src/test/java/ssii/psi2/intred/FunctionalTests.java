package ssii.psi2.intred;

import org.junit.Test;
import static org.junit.Assert.*;
import javax.net.SocketFactory;
import java.io.*;
import java.net.Socket;
//import java.io.File;

public class FunctionalTests {
    @Test
    public void serverClientFunctionalTest() throws Exception {
        IntegrityVerifierServer server = new IntegrityVerifierServer("MyKey", "HmacSHA256", 7070);
        Thread serverThread = new Thread(server);
        ClientConnection clientConnection = null;
        try {
            serverThread.start();

            clientConnection = new ClientConnection(
                    SocketFactory.getDefault().createSocket("localhost", 7070),
                    new MessageIntegrityVerifier("MyKey", "HmacSHA256"),
                    new NonceGenerator()
            );
            assertTrue(clientConnection.sendRequestOperation("Hello from the simple functional test!"));
        } finally {
            if (clientConnection != null)
                clientConnection.close();
            server.close();
            serverThread.join();

//            File logFile = new File(server.getLogFilePath());
//            if (logFile.exists())
//                assertTrue(logFile.delete());
        }
    }

    @Test
    public void keyMissmatchServerFunctionalTest() throws Exception {
        IntegrityVerifierServer server = new IntegrityVerifierServer("MyKey1", "HmacSHA256", 7070);
        Thread serverThread = new Thread(server);
        ClientConnection clientConnection = null;
        try {
            serverThread.start();

            clientConnection = new ClientConnection(
                    SocketFactory.getDefault().createSocket("localhost", 7070),
                    new MessageIntegrityVerifier("MyKey2", "HmacSHA256"),
                    new NonceGenerator()
            );
        } catch (IntredException ie) {
            assertEquals(IntredException.ErrorCode.INTEGRITY_ERROR, ie.getErrorCode());
        }
        finally {
            if (clientConnection != null)
                clientConnection.close();
            server.close();
            serverThread.join();

//            File logFile = new File(server.getLogFilePath());
//            if (logFile.exists())
//                assertTrue(logFile.delete());
        }
    }

    @Test
    public void dayNonceMismatchServerFunctionalTest() throws Exception {
        IntegrityVerifierServer server = new IntegrityVerifierServer("MyKey", "HmacSHA256", 7070);
        Thread serverThread = new Thread(server);

        Socket clientSocket = null;
        BufferedReader input = null;
        BufferedWriter output = null;
        MessageIntegrityVerifier clientIntegrityVerifier = new MessageIntegrityVerifier("MyKey", "HmacSHA256");
        try {
            serverThread.start();

            clientSocket = SocketFactory.getDefault().createSocket("localhost", 7070);
            input = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            output = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));

            // Leer mensaje de bienvenida
            String welcomeMessage = input.readLine();
            if (welcomeMessage == null)
                throw new EOFException("Server closed connection unexpectedly.");
            String mac = input.readLine();
            if (mac == null)
                throw new EOFException("Server closed connection unexpectedly.");

            // Comprobar integridad
            if (!clientIntegrityVerifier.checkMessage(welcomeMessage, mac))
                throw new Exception("Integrity error!");

            int receivedDayNonce = getDayNonceFromWelcomeMessage(welcomeMessage);
            String challengeNonce = getChallengeNonceFromWelcomeMessage(welcomeMessage);

            String messageWithInvalidDayNonce = String.valueOf(receivedDayNonce-1) + " " + challengeNonce + " 1 REQUEST This has an invalid dayNonce";
            output.write(messageWithInvalidDayNonce + "\n");
            output.write(clientIntegrityVerifier.getHMac(messageWithInvalidDayNonce) + "\n");
            output.flush();

            assertNull(input.readLine());
        } finally {
            if (input != null)
                input.close();
            if (output != null)
                output.close();
            if (clientSocket != null && !clientSocket.isClosed())
                clientSocket.close();

            server.close();
            serverThread.join();
//            File logFile = new File(server.getLogFilePath());
//            if (logFile.exists())
//                assertTrue(logFile.delete());

        }
    }

    @Test
    public void challengeNonceMismatchServerFunctionalTest() throws Exception {
        IntegrityVerifierServer server = new IntegrityVerifierServer("MyKey", "HmacSHA256", 7070);
        Thread serverThread = new Thread(server);

        Socket clientSocket = null;
        BufferedReader input = null;
        BufferedWriter output = null;
        MessageIntegrityVerifier clientIntegrityVerifier = new MessageIntegrityVerifier("MyKey", "HmacSHA256");
        try {
            serverThread.start();

            clientSocket = SocketFactory.getDefault().createSocket("localhost", 7070);
            input = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            output = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));

            // Leer mensaje de bienvenida
            String welcomeMessage = input.readLine();
            if (welcomeMessage == null)
                throw new EOFException("Server closed connection unexpectedly.");
            String mac = input.readLine();
            if (mac == null)
                throw new EOFException("Server closed connection unexpectedly.");

            // Comprobar integridad
            if (!clientIntegrityVerifier.checkMessage(welcomeMessage, mac))
                throw new Exception("Integrity error!");

            int receivedDayNonce = getDayNonceFromWelcomeMessage(welcomeMessage);
            String challengeNonce = getChallengeNonceFromWelcomeMessage(welcomeMessage);

            String messageWithInvalidDayNonce = String.valueOf(receivedDayNonce) + " NotTheChallengeNonce 1 REQUEST This has an invalid challengeNonce";
            output.write(messageWithInvalidDayNonce + "\n");
            output.write(clientIntegrityVerifier.getHMac(messageWithInvalidDayNonce) + "\n");
            output.flush();

            assertNull(input.readLine());
        } finally {
            if (input != null)
                input.close();
            if (output != null)
                output.close();
            if (clientSocket != null && !clientSocket.isClosed())
                clientSocket.close();

            server.close();
            serverThread.join();
//            File logFile = new File(server.getLogFilePath());
//            if (logFile.exists())
//                assertTrue(logFile.delete());

        }
    }

    @Test
    public void invalidMacFromClientServerFunctionalTest() throws Exception {
        IntegrityVerifierServer server = new IntegrityVerifierServer("MyKey", "HmacSHA256", 7070);
        Thread serverThread = new Thread(server);

        Socket clientSocket = null;
        BufferedReader input = null;
        BufferedWriter output = null;
        MessageIntegrityVerifier clientIntegrityVerifier = new MessageIntegrityVerifier("MyKey", "HmacSHA256");
        try {
            serverThread.start();

            clientSocket = SocketFactory.getDefault().createSocket("localhost", 7070);
            input = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            output = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));

            // Leer mensaje de bienvenida
            String welcomeMessage = input.readLine();
            if (welcomeMessage == null)
                throw new EOFException("Server closed connection unexpectedly.");
            String mac = input.readLine();
            if (mac == null)
                throw new EOFException("Server closed connection unexpectedly.");

            // Comprobar integridad
            if (!clientIntegrityVerifier.checkMessage(welcomeMessage, mac))
                throw new Exception("Integrity error!");

            int receivedDayNonce = getDayNonceFromWelcomeMessage(welcomeMessage);
            String challengeNonce = getChallengeNonceFromWelcomeMessage(welcomeMessage);

            String messageWithInvalidDayNonce = String.valueOf(receivedDayNonce) + " " + challengeNonce + " 1 REQUEST This will fail the integrity check";
            output.write(messageWithInvalidDayNonce + "\n");
            output.write(clientIntegrityVerifier.getHMac(messageWithInvalidDayNonce + " something to make mac different") + "\n");
            output.flush();

            assertNull(input.readLine());
        } finally {
            if (input != null)
                input.close();
            if (output != null)
                output.close();
            if (clientSocket != null && !clientSocket.isClosed())
                clientSocket.close();

            server.close();
            serverThread.join();
//            File logFile = new File(server.getLogFilePath());
//            if (logFile.exists())
//                assertTrue(logFile.delete());

        }
    }

    private static int getDayNonceFromWelcomeMessage(String message) throws Exception {
        String [] messageWords = message.split(" ");
        if (messageWords.length != 4)
            throw new Exception("Incorrect number of words!");
        if (!messageWords[0].equals("200") || !messageWords[1].equals("HELLO"))
            throw new Exception("Incorrect format!");

        try { return Integer.parseInt(messageWords[2]); }
        catch (Exception e) { throw new Exception("DayNonce is not an integer!"); }
    }

    private static String getChallengeNonceFromWelcomeMessage(String message) throws Exception {
        String [] messageWords = message.split(" ");
        if (messageWords.length != 4)
            throw new Exception("Incorrect number of words!");
        if (!messageWords[0].equals("200") || !messageWords[1].equals("HELLO"))
            throw new Exception("Incorrect format!");

        return messageWords[3];
    }
}
